$fn=100;

module dist(x, y, z, pattern) {
  if (pattern == "xyz") {
    distx(x) disty(y) distz(z) children();
  } else if (pattern == "xy") {
    trxyz(0, 0, z) distx(x) disty(y) children();
  } else if (pattern == "xz") {
    trxyz(0, y, 0) distx(x) distz(z) children();
  } else if (pattern == "yz") {
    trxyz(x, 0, 0) disty(y) distz(z) children();
  } else if (pattern == "x") {
    trxyz(0, y, z) distx(x) children();
  } else if (pattern == "y") {
    trxyz(x, 0, z) disty(y) children();
  } else if (pattern == "z") {
    trxyz(x, y, 0) distz(z) children();
  }
}

module distx(x, m = 0) {
  for (i = [x, -x]) {
    sca = ((m == 1) && (i == -x)) ? -1 : 1;
    trxyz(i, 0, 0) scale([sca, 1, 1]) children();
  }
}

module disty(y, m = 0) {
  for (i = [y, -y]) {
    sca = ((m == 1) && (i == -y)) ? -1 : 1;
    trxyz(0, i, 0) scale([1, sca, 1]) children();
  }
}

module distxz(x, z) {
  distx(x) distz(z) children();
}

module distxy(x, y) {
  distx(x) disty(y) children();
}

module distyz(y, z) {
  disty(y) distz(z) children();
}

module distz(z) {
  for (i = [z, -z]) trxyz(0, 0, i) children();
}
module corners(x, y) { dist(x, y, 0, "xy") children(); }

module rx(a) { rotate([a, 0, 0]) children(); }
module ry(a) { rotate([0, a, 0]) children(); }
module rz(a) { rotate([0, 0, a]) children(); }

module tx(a) { translate([a, 0, 0]) children(); }
module ty(a) { translate([0, a, 0]) children(); }
module tz(a) { translate([0, 0, a]) children(); }

module trxyz(x, y, z) { translate([x, y, z]) children(); }

module cubec(li) { cube(li, center = true); }
module cylc(dia, hei) { cylinder(d = dia, h = hei, center = true); }
module ring(outerd, innerd, height) {
  difference() {
    cylinder(d = outerd, h = height, center = true);
    cylinder(d = innerd, h = height+1, center = true);
  }
}
